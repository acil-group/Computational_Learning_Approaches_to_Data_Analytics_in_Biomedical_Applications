function [norm_data]=normalize_dset(raw_data)
% this function normalized data set based on min-max
[n,ftrs]=size(raw_data);
norm_data=zeros(n,ftrs);
% [x,y]=find(~isnumeric(raw_data));
for i=1:ftrs
    tmp=raw_data(:,i);
    if sum(tmp)>0
%         for j=1:length(nsids)
%             tmp(nsids(j))=mean(tmp);
%         end
        ftrmin=min(tmp);
        ftrmax=max(tmp);
        norm_data(:,i)=(tmp-ftrmin)/(ftrmax-ftrmin);
    else
        disp(['feature: (',num2str(i), ') is an empty feature']);
    end
end

end