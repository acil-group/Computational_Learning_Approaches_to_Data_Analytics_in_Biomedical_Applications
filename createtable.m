function [T_out] = createtable(fname,varF,format,opt,dlm )
%This function is to create a table object  
%   This function is designed to provide a multiple formats for reading data files into formatted 
% table, this function was developed to support the material in Chapter11
% from "Computational Learning Approaches to Data Analytics in Biomedical
% Applications" cite: ... to be specified later
% fname: is input file name
% varF: flag to indicate whether to read the variables name from the table
% or not. format: columns format inside the data file. dlm: delimiter. 
% opt: is optional input used only with Matlab 2017 and later.
% code written by Khalid Aljabery, Phd candidate at Missouri S&T department
% of electrical and computers engineering Dec. 31st, 2017.
excellF=strcmp(fname(end-5:end),'.xlsx') || strcmp(fname(end-4:end),'.xls');
if ~ischar(dlm)
    [~,dlm]=importdata(fname);
end
if ~isempty(opt) && excellF
    T_out=readtable(fname,opt);
elseif varF
    T_out=readtable(fname,'Delimiter',dlm,'format',format);
else
    T_out=readtable(fname,'ReadVariableNames',false,'Delimiter',dlm,'format',format);
end

end

