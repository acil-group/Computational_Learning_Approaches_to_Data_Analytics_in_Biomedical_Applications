
# coding: utf-8

# In[14]:


import os
import sys
try:
    with open('test.txt','r')as file:
        data=file.read()
        print(data)
        
except:
    print('I/O error')
finally:
    print('example complete')


# In[12]:


f=open('test.txt')
f.readline()


# In[13]:


f.read(5)


# In[16]:


f.seek(5)


# In[11]:


f.readline()


# In[17]:


f.close()

