function [labels]=find_HIC(inpdata,par,k)
% Hierarchical clustering
% inpdata: is the full dataset
% par: type of linkage 
% k the cut level
Y=pdist(inpdata,'euclid');% find pairwise distance 
z=linkage(Y,par);% generate linkage model 
figure;
dendrogram(z,79)% show dendrogram
if strcmp(par,'ward')
   ylim([0 10]);
else
   ylim([0 0.4]);
end
title(['Hierarchical clustering for the data before removing any outliers using ',par,' linkage']);
c=2:1:k;
%% this section is optional for multiple clustering criteria
% c=zeros(1,k-1);
% for i=1:length(c)
%     c(i)=i+1;
% end
%%   
T = cluster(z,'maxclust',c);
labels=T;
end
