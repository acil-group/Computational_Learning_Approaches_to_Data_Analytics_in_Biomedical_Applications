%% Import & intialize data
data = readtable('BreastTissue.csv');
data.Class = categorical(data.Class);
% Get numeric columns and normalize them
stats = data{:,3:end};
labels = data.Properties.VariableNames(3:end);
statsNorm = zscore(stats);
% Reconstruct coordinates
[pcs,scrs,~,~,pexp] = pca(statsNorm);
% show the pca importance
pareto(pexp);
title('PCA visualization');
%% calculate multidimensional scalling
D=pdist(statsNorm);
[X,E]=mdscale(D,3);
figure;
pareto(E);
title('Eigenvalues of mdscale');
%% Group data using GMM
gmModel = fitgmdist(statsNorm,2,'Replicates',5,...
    'RegularizationValue',0.02);
[grp,~,gprob] = cluster(gmModel,statsNorm);
%% View data
% Visualize groups
figure;
scatter3(scrs(:,1),scrs(:,2),scrs(:,3),20,grp,...
    'filled','MarkerEdgeColor','k')
view(110,40)
xlabel('PCA1');ylabel('PCA2');zlabel('PCA3');
title('3-D visualisation of the resulted clusters');
% Visualize group separation
gpsort = sortrows(gprob,1);
figure;
plot(gpsort,'LineWidth',4)
xlabel('Point Ranking')
ylabel('Cluster Membership Probability')
legend('Cluster 1','Cluster2')
title('Membership distribution of data samples')
figure;
parallelcoords(stats,'Group',grp,'Quantile',0.25)
title('Clusters visualization using parallelcoords');
legend('Cluster1','Cluster2');
