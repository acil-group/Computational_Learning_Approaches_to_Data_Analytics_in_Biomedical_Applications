%% example read dataset and cluster them using kmeans
%1- using default settings
cl_idx=kmeans(normalized_data,2);
gscatter(pcad(:,1),pcad(:,2),cl_idx,'rb','++')
%2- using tuned options 
figure;
cl_idx=kmeans(normalized_data,2,'Distance','correlation','Replicates',5);
gscatter(pcad(:,1),pcad(:,2),cl_idx,'rb','++')

