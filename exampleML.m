%% Load the data
heartData = readtable('heartDataAll.txt','Format',[repmat('%f',1,11),repmat('%C',1,11)]);
numData = heartData{:,1:11};

% Extract the response variable
HD = heartData.HeartDisease;
% Make a partition for evaluation
rng(1234)
part = cvpartition(HD,'KFold',10);

%% Convert categorical variables to numeric dummy variables
[X,Xnames] = cattable2mat(heartData);

%% Fit a Naive Bayes model to the full data
dists = [repmat({'kernel'},1,11),repmat({'mvmn'},1,10)];
mFull = fitcnb(heartData,'HeartDisease','Distribution',dists,'CVPartition',part);

%% Perform sequential feature selection
rng(1234)
fmodel = @(X,y) fitcnb(X,y,'Distribution','kernel');
ferror = @(Xtrain,ytrain,Xtest,ytest) nnz(predict(fmodel(Xtrain,ytrain),Xtest) ~= ytest);
tokeep = sequentialfs(ferror,X,HD,'cv',part,...
    'options',statset('Display','iter'));

% Which variables are in the final model?
Xnames(tokeep)

% Fit a model with just the given variables
mPart = fitcnb(X(:,tokeep),HD,'Distribution','kernel','CVPartition',part);

% Display loss values
disp([num2str(kfoldLoss(mFull)),' = loss with all data'])
disp([num2str(kfoldLoss(mPart)),' = loss with selected data'])
