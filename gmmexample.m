%% GMM example on plants dataset
tbl=readtable('parkinsons_updrs.csv');% read dataset
data=tbl{:,4:end};
norm_data=normalize_dset(data);% normalize dataset 
gmm=fitgmdist(norm_data,2,'RegularizationValue',0.1);% generate the gm model
g=cluster(gmm,norm_data);% generate labels
[exp,pcad]=pca(norm_data);% generate pca 
gscatter(pcad(:,1),pcad(:,2),g,'rb','++')% visualize data
