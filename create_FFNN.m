function [val_err,FFNN]=create_FFNN(data,tg,p_val,p_test)
%% this function creates a typical FFNN  
%X: full data set; tg: responses
% p_val: validation ratio (i.e., 0.15), p_test: test ratio
% FFNN is a pattern recognition , val_err is validation error
n=length(unique(tg));% find number of classes
net = patternnet(n);
X=data';tg=tg';
net.divideParam.trainRatio = 1-p_val-p_test;
net.divideParam.valRatio = p_val;
net.divideParam.testRatio = p_test;
[net,tr] = train(net,X,tg);
scoreTest = net(X(:,tr.testInd));
tgtest=tg(tr.testInd);
[~,yPred] = max(scoreTest);
plotconfusion(tgtest,yPred);
val_err=100*nnz(yPred ~= double(tgtest))/length(tgtest);
FFNN=net;
end