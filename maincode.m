%% this code is for the paper with Majid the concentration of compounds
% read the data

tscf=readtable('S:\khalid\Majid\paper1\newruninp.xlsx');
tscf.numericplant=encode_features(tscf.Plant);
tscf.encoded_ch=encode_features(tscf.Chemical);
% normalization
% paper2
mat=[tscf.numericplant,tscf.encoded_ch,convertcell2array(tscf.Log_Kow_),convertcell2array(tscf.MW),convertcell2array(tscf.HBA),...
    convertcell2array(tscf.HBD),convertcell2array(tscf.PSA),convertcell2array(tscf.RB)];
% this is for paper1
% mat=[tscf.Log_Kow_,tscf.HBA,tscf.HBD,tscf.MW,tscf.PSA,tscf.RB];
normalized_data=normc(mat);
kmeanslbl=implement_kmeans(normalized_data,'kmeans',5);
kmedoidslbl=implement_kmeans(normalized_data,'kmedoids',5);
kmeanseval=find_internal_index(kmeanslbl,normalized_data);
kmedoidseval=find_internal_index(kmedoidslbl,normalized_data);
tscf.kmeanslbl=kmeanslbl;
tscf.kmedoidslbl=kmedoidslbl;
HIClbl=find_HIC(normalized_data,'complete',5);
HIC_comp_val=find_internal_index(HIClbl,normalized_data);
HIClblw=find_HIC(normalized_data,'ward',5);
HIC_ward_val=find_internal_index(HIClblw,normalized_data);
kmns1=[kmeanslbl;kmeanseval];
kmed1=[kmedoidslbl;kmedoidseval];
HIC1=[[HIClbl;HIC_comp_val],[HIClblw;HIC_ward_val]];
[~,dpca,~]=pca(normalized_data);
%%storing data
xlswrite('S:\khalid\Majid\labels.xlsx',[kmns1,kmed1],1);
xlswrite('S:\khalid\Majid\labels.xlsx',HIC1,2);
xlswrite('S:\khalid\Majid\paper1\pca.xlsx',normalized_data,1);
xlswrite('S:\khalid\Majid\paper1\pca.xlsx',dpca,2);









