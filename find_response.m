function cellidx=find_response(tblvarnames,response)
%% this function finds a cell in a cell array
cellidx=[];
for i=1:length(tblvarnames)
    if strcmpi(tblvarnames{i},response)
        cellidx=i;
        break;
    end
    
end
if isempty(cellidx)
    disp('The response name you have enterred is not available in table');
    keyboard;
end
end