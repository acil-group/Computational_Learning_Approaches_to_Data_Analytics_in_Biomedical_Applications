
# coding: utf-8

# In[25]:


from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
data=pd.read_csv('S:\\khalid\\the book\\CH11\\S2_Dataset\\d2p01F.csv')
n_data=data
kmeans = KMeans(n_clusters=2, random_state=0,).fit(n_data)
pca=PCA(n_components=2).fit(n_data)
data_2d = pca.transform(n_data)
y_kmeans = kmeans.predict(n_data)
plt.scatter(data_2d[:,0],data_2d[:,1], c=y_kmeans,s=50, cmap='viridis')
plt.show()

