function error = errorFun(Xtrain,ytrain,Xtest,ytest)
% Create the model with the learning method of your choice
mdl = fitcsvm(Xtrain,ytrain);
% Calculate the number of test observations misclassified
ypred = predict(mdl,XTest);
error = nnz(ypred ~= ytest);
end