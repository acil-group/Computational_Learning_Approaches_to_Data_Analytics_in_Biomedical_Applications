function data_in_cellformat=readfiles_using_textscan(filename,option)
% this function is designed as an example for section 2.1.3 in chapter11
% from the Application of machine learning in biomedical applications
% Code written by Khalid Aljabery , khpcengineer@gmail.com
fid=fopen(filename);
% for matlab2017a and higher use 
% opt=detectImportOptions(filename);
% data=textscan(fid,opt);
switch option
    case 'standard'
        
