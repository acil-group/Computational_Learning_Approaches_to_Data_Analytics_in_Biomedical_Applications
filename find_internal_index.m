function val_indices=find_internal_index(lblin,datainp)
%% this function designed to find the internal validation indices CHI, SI, DB
% for multiple clustering criterias
% inputs: lblin: matrix of labels (n x k)each column represents clustering
% criteria, datainp: input data.
% outputs: 3 x k array each entry represents a validation index for the
% corresponding clustering scheme
val_indices=[];
for i=1:size(lblin,2)
    lbl=lblin(:,i);
    evdb=evalclusters(datainp,lbl,'DaviesBouldin');
    DBI=evdb.CriterionValues;
    evSI=evalclusters(datainp,lbl,'silhouette');
    SI=evSI.CriterionValues;
    evCH=evalclusters(datainp,lbl,'CalinskiHarabasz');
    CHI=evCH.CriterionValues;
    val_indices=[val_indices,[DBI;SI;CHI]];
end
end