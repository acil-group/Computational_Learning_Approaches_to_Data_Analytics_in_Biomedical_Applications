function [traininginds,testinds]=prepare_data(response,p)
%% this function returns the indices for the training and test datasets
% response: is categorical vector contains classes of observations
% p: the percentage of test set in decimal format e.g. %35 = 0.35
cvpm=cvpartition(response,'HoldOut',p);
traininginds=training(cvpm);
testinds=test(cvpm);
end
