function [mdl,training_err, test_err]=classify_fit_demo(data,response,fitc,validation_type)
%% This function is a generalized form of all fitting functions in MATLAB
% it summarizes section 11.4.2
%% Inputs:
% data: is the complete dataset
% fitc: a character variable that represents the type of fitting function
% validation type: type of validation method kfold or train-test subset
% response: index of the vector variable if data is a table this variable is just
% the name of the response variable
% the index of the response variable in the table, but if the data is an
% array then
% tbl: flag indicating if the data is a table or not
%% outputs:
% mdl: generated classification model
% training_err: in case of using normal partitioning
% test_err: either kfold or test
% code written by Khalid Aljabery for feedback and comments email:
% khpcengineer@gmail.com
%%
if istable(data)% find the location of the response variable
    names=data.Properties.VariableNames;
    respid=find_response(names,response);
    data.class=categorical(data{:,respid});%store class vector at the end of the table
    data(:,respid)=[];%delete class vector
    cls=data.class;
    inpdata=data{:,1:end-1};% create a numeric array from the table
else % if dataset fed as an array
    cls=data(:,response);
    data(:,response)=[];
end

if strcmpi(validation_type,'kfold')
    k=' ';
    while ~isnumeric(k)
        k=input('Please, enter the number of folds you want to divide the data to:');
    end
    pts=cvpartition(cls,'KFold',k);
    switch fitc
        case 'SVM'
            if length(unique(cls))<=2 % binary classifier
                mdl=fitcsvm(inpdata,cls,'CVPartition',pts);
            else % multi-class svm
                kernel=input('Please enter the kernel function (rbf, polynomial, linear (default)):','s'); 
                tmp=templateSVM('KernelFunction',kernel);
                mdl=fitcecoc(inpdata,cls,'CVPartition',pts,'Learners',tmp);
            end
        case 'KNN'
            mdl=fitcknn(inpdata,cls,'CVPartition',pts);
        case 'NB'
            mdl=fitcnb(inpdata,cls,'CVPartition',pts);
        case 'TREE'
            mdl=fitctree(inpdata,cls,'CVPartition',pts);
        case 'DISCR'
            mdl=fitcdiscr(inpdata,cls,'CVPartition',pts);
        case 'Linear'
            mdl=fitclinear(inpdata,cls,'CVPartition',pts);
        otherwise
            disp('You have enterred the wrong fitting function');
            return;
    end 
    training_err=kfoldLoss(mdl);
    disp('Cross-validation error generated');
else % normal test and training division
    p=' ';
    while ~isnumeric(p)
        p=input('Please, enter the validation fraction:');
    end
    pts=cvpartition(cls,'HoldOut',p);
    trainingidx=training(pts);
    testidx=test(pts);
    trn_data=data(trainingidx,:);
    tst_data=data(testidx,:);
    y_trn=cls(trainingidx);
    y_tst=cls(testidx);
    switch fitc
        case 'SVM'
            if length(unique(cls))<=2 % binary classifier
                mdl=fitcsvm(trn_data,y_trn);
            else % multi-class svm
                mdl=fitcecoc(trn_data,y_trn);
            end
        case 'KNN'
            mdl=fitcknn(trn_data,y_trn);
        case 'NB'
            mdl=fitcnb(trn_data,y_trn);
        case 'TREE'
            mdl=fitctree(trn_data,y_trn);
        case 'DISCR'
            mdl=fitcdiscr(trn_data,y_trn);
        case 'Linear'
            mdl=fitclinear(trn_data,y_trn);
        otherwise
            disp('You have enterred the wrong fitting function');
            return;
    end     
    training_err=resubLoss(mdl);
    test_err=loss(mdl,tst_data,y_tst);  
end
    
    
    
