function somnet=create_SOM(TR_data,sz, No_epochs)
%% this function creates, train and plot SOM
% inputs:- 
% TR_data training data, sz =[m x n] SOM dimensions,
% No_epochs:number of times repeat training 
% outputs:- somnet is the self-organizing map NN ready to be used on new data
net = selforgmap(sz);
net.epochs = No_epochs;
X = TR_data';
net = train(net,X);
plotsomhits(net,X)
somnet=net;
end