function [mdl,training_err, test_err]=classify_fit_demo(data,response,fitc,validation_type)
%% This function is a generalized form of all fitting functions in MATLAB
% it summarizes section 11.4.2
%% Inputs:
% data: is the complete dataset
% fitc: a character variable that represents the type of fitting function
% validation type: type of validation method kfold or train-test subset
% response: class vector variable if data is a table this variable is just
% the index of the response variable in the table, but if the data is an
% array then 
% tbl: flag indicating if the data is a table or not
%% outputs:
% mdl: generated classification model
% training_err: in case of using normal partitioning
% test_err: either kfold or test
%%
if tbl
    resp=data.response{:}
if strcmpi(validation_type,'kfold')
    k=' ';
    while ~isnumeric(k)
        k=input('Please, enter the number of folds you want to divide the data to:');
    end
    pts=cvpartition(response,'KFold',k);
    
else
    p=' ';
    while ~isnumeric(p)
        p=input('Please, enter the validation fraction:');
    end
    pts=cvpartition(response,'HoldOut',p);
    trainingidx=training(pts);
    testidx=test(pts);
end
switch fitc
    case 'SVM'
        
        
        
