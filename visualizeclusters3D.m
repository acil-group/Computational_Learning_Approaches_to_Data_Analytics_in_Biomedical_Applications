%% 3d visualization
function visualizeclusters3D(lbls,pcadata,strtitle)
%% this function will generate a 3dimensional visualization for the labels
% like in Excel number of clusters should not be more than 6
cls=unique(lbls);% find number of clusters
clrs={'r','b','g','m','k','c'};
figure;
visdata=pcadata(:,1:3);

for i=1:length(cls)
    clsi=visdata(lbls==i,:);
    scatter3(clsi(:,1),clsi(:,2),clsi(:,3),char(clrs{i}),'Filled')
    hold on
end
title(strtitle);
end
